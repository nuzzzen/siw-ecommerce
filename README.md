<h1 style="text-align: center;"> Sistemi Informativi su Web - Progetto e-Commerce </h1>

<div align="center">
  <img src="src/main/resources/static/images/png/logo-color.png" alt="siw-ecommerce-logo" width="500" height="500">
</div>

## Specifiche assegnate dal docente

Si vuole realizzare il sistema informativo su Web per la gestione di un catalogo di prodotti.\
Possono accedere al sistema **utenti generici** e **un amministratore**. \
Il catalogo contiene l’elenco dei prodotti e i fornitori (uno o più) di ciascun prodotto.

- L’amministratore, previa autenticazione,
  può modificare i dati di un prodotto e i dati di un fornitore. In particolare, l’amministratore può
  inserire, rimuovere, aggiornare un prodotto, modificare l’elenco dei fornitori di un prodotto (cioè
  può aggiungere o rimuovere un fornitore a un prodotto), aggiungere un fornitore.


- Per ogni prodotto sono di interesse il nome, il prezzo, una descrizione, l’elenco dei fornitori. Per
  ogni fornitore sono di interesse il nome, l’indirizzo, l’email. L’associazione tra prodotto e fornitore
  è **molti a molti**.


- L’utente generico può accedere alle informazioni del catalogo attraverso diversi percorsi di
  navigazione, opportunamente predisposti (ad esempio, per fornitore, oppure per nome, etc.) e
  può inserire un commento su un prodotto. I commenti degli utenti sono visibili a tutti gli utenti.
  Ogni utente deve poter modificare solo i propri commenti.

> Implementare il sistema in Java con il framework Spring Boot.

## Implementazioni da terminare

- [x] Better home page
- [x] Securizzare i path `/admin/**`.
- [ ] CSS all'interno di un file unico
- [x] Rimozioni commenti solo da admin o stesso utente
- [ ] (Opzionale) Aggiunta immagini prodotti
- [ ] (Opzionale) Creazione DTO per User
- [ ] (Opzionale) Ripachettizzazione dei file del progetto

## Known bugs

- Nessuno al momento.

<details>
  <summary>Altro</summary>
Progettare il proprio sistema, definendone casi d’uso, modello di dominio (con indicazioni utili alla progettazione dello strato di persistenza)
Implementare almeno 4 casi d’uso:
- almeno uno che richieda operazioni di **inserimento** dei dati di una entità
- almeno uno che richieda **aggiornamento** dei dati di una entità
- almeno due che richiedano operazioni di **lettura** dei dati di una o più entità
- Implementare il sistema con Spring Boot.
- Il deploy su una piattaforma cloud (ad esempio, **AWS**, Azure, o Heroku) e l’autenticazione tramite Oauth saranno considerati un plus


Saranno oggetto di valutazione anche la **qualità del codice** e la qualità dell’interfaccia HTML+CSS (è possibile usare
librerie CSS come, ad esempio, Boostrap).
> NB: autenticazione e registrazione dell'utente **non sono considerati casi d'uso**.
</details>
