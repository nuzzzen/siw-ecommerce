package it.uniroma3.siw.ecommerce.repository;

import it.uniroma3.siw.ecommerce.model.Comment;
import it.uniroma3.siw.ecommerce.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {
    Boolean existsByAuthorAndId(User author, UUID id);

    Optional<Comment> findByAuthor(User author);
}
