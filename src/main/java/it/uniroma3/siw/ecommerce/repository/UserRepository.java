package it.uniroma3.siw.ecommerce.repository;

import it.uniroma3.siw.ecommerce.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    Boolean existsByEmail(String email);

    Optional<User> findUserById(UUID id);

    Optional<User> findUserByEmail(String email);

    Optional<User> findUserByUsername(String username);


    Boolean existsByUsername(String username);
}
