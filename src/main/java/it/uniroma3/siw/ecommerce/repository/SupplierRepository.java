package it.uniroma3.siw.ecommerce.repository;

import it.uniroma3.siw.ecommerce.model.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface SupplierRepository extends JpaRepository<Supplier, UUID> {
    Boolean existsByNameAndEmail(String name, String email);

    Optional<Supplier> findSupplierById(UUID id);

    Optional<Supplier> findByName(String name);


    boolean existsByName(String name);
}
