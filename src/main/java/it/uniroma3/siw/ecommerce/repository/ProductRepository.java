package it.uniroma3.siw.ecommerce.repository;

import it.uniroma3.siw.ecommerce.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ProductRepository extends JpaRepository<Product, UUID> {
    Boolean existsByNameAndId(String name, UUID id);

    Optional<Product> findProductByName(String name);

    Optional<Product> findByBrandAndName(String brand, String name);

}
