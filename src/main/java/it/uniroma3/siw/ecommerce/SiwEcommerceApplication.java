package it.uniroma3.siw.ecommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SiwEcommerceApplication {

	public static void main(String[] args) {
        SpringApplication.run(SiwEcommerceApplication.class, args);
	}

}
