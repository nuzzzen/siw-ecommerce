package it.uniroma3.siw.ecommerce.authentication;

import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

import static it.uniroma3.siw.ecommerce.enumeration.users.UserRole.ADMIN_ROLE;
import static it.uniroma3.siw.ecommerce.enumeration.users.UserRole.DEFAULT_ROLE;

@Configuration
@EnableWebSecurity
@AllArgsConstructor
@Log4j2
public class AuthConfig {

    private final DataSource dataSource;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @PostConstruct
    public void configureGlobal() throws Exception {
        authenticationManagerBuilder.jdbcAuthentication()
                .dataSource(dataSource)
                .authoritiesByUsernameQuery("SELECT username, user_role from user WHERE username=?")
                .usersByUsernameQuery("SELECT username, password, 1 as enabled FROM user WHERE username=?");
        log.info("Data source: {}", dataSource);
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration)
            throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    @Bean
    public SecurityFilterChain securityFilterChain(final HttpSecurity http)
            throws Exception {
        http.csrf().disable()
                .authorizeHttpRequests((authorize) ->
                        authorize.requestMatchers(HttpMethod.GET,
                                        "/",
                                        "/index",
                                        "/register/**",
                                        "/login/**",
                                        "/css/**",
                                        "/images/**",
                                        "/favicon.ico",
                                        "/products/**",
                                        "/product/**",
                                        "/comments/**",
                                        "/supplier/**",
                                        "/suppliers/**").permitAll()
                                .requestMatchers(HttpMethod.POST, "/register", "/login").permitAll()
                                .requestMatchers(HttpMethod.POST, "/comments/**")
                                .hasAnyAuthority(ADMIN_ROLE.getAuthority(), DEFAULT_ROLE.getAuthority())
                                .requestMatchers(HttpMethod.GET, "/admin/**").hasAuthority(ADMIN_ROLE.getAuthority())
                                .requestMatchers(HttpMethod.POST, "/admin/**").hasAuthority(ADMIN_ROLE.getAuthority())
                                .anyRequest().authenticated()
                                .and()
                ).formLogin(
                        form -> form
                                .loginPage("/login")
                                .loginProcessingUrl("/login")
                                .defaultSuccessUrl("/index", true)
                                .permitAll()
                                .and()
                ).logout(
                        logout -> logout
                                .logoutUrl("/logout")
                                .logoutSuccessUrl("/login")
                                .invalidateHttpSession(true)
                                .deleteCookies("JSESSIONID")
                                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                                .clearAuthentication(true)
                                .permitAll()
                );
        return http.build();
    }
}
