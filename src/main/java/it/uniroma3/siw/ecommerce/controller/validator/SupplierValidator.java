package it.uniroma3.siw.ecommerce.controller.validator;

import it.uniroma3.siw.ecommerce.model.Supplier;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
@RequiredArgsConstructor
public class SupplierValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Supplier.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "supplier.name.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "supplier.email.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "supplier.address.empty");
    }
}

