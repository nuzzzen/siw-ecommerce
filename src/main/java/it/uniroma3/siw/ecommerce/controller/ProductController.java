package it.uniroma3.siw.ecommerce.controller;

import it.uniroma3.siw.ecommerce.controller.validator.ProductValidator;
import it.uniroma3.siw.ecommerce.model.Product;
import it.uniroma3.siw.ecommerce.model.Supplier;
import it.uniroma3.siw.ecommerce.service.ProductService;
import it.uniroma3.siw.ecommerce.service.SupplierService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Controller
@RequiredArgsConstructor
@Log4j2
public class ProductController {
    //TODO use validators

    private final ProductService productService;
    private final SupplierService supplierService;
    private final ProductValidator productValidator;

    @GetMapping("/product/{id}")
    public String getProduct(@PathVariable("id") UUID id, Model model) {
        model.addAttribute("product", productService.getProductById(id));
        return "product";
    }

    @GetMapping("/products")
    public String getAllProducts(Model model) {
        model.addAttribute("products", productService.getAllProducts());
        return "products";
    }

    @GetMapping("/admin/createProduct")
    public String createProductForm(Model model) {
        model.addAttribute("product", new Product());
        model.addAttribute("suppliers", supplierService.getAllSuppliers());
        return "admin/createProduct";
    }

    @PostMapping("/admin/products/create")
    public String createProduct(
            @ModelAttribute("product") @Valid Product product,
            BindingResult bindingResult,
            @RequestParam(name = "suppliersList", required = false) List<UUID> supplierIds,
            Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("error", true);
            model.addAttribute("suppliers", supplierService.getAllSuppliers());
            return "admin/createProduct";
        }

        if (supplierIds != null && !supplierIds.isEmpty()) {
            for (UUID supplierId : supplierIds) {
                Supplier supplier = supplierService.getSupplierById(supplierId);
                supplier.getProductsList().add(product);
                supplierService.updateSupplier(supplier);
            }
        }

        productService.createProduct(product);

        return "redirect:/product/" + product.getId();
    }

    @PostMapping("/admin/products/delete/{id}")
    public String postDeleteProduct(@PathVariable("id") UUID id) {
        return deleteProduct(id);
    }

    @DeleteMapping("/admin/products/delete/{id}")
    public String deleteProduct(@PathVariable("id") UUID id) {
        productService.deleteProduct(id);
        return "redirect:/admin/productsDashboard";
    }

    @GetMapping("/admin/products/edit/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostAuthorize("hasRole('ROLE_ADMIN')")
    public String getUpdateProductForm(@PathVariable("id") UUID id, Model model) {
        Product product = productService.getProductById(id);
        model.addAttribute("product", product);
        model.addAttribute("suppliers", supplierService.getAllSuppliers());
        return "admin/editProduct";
    }

    @PostMapping("/admin/products/edit/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostAuthorize("hasRole('ROLE_ADMIN')")
    public String updateProduct(
            @PathVariable("id") UUID id,
            @Valid @ModelAttribute("product") Product updatedProduct,
            BindingResult bindingResult,
            Model model,
            @RequestParam(name = "supplierIds", required = false) List<UUID> supplierIds) {

        if (!bindingResult.hasErrors()) {
            Product existingProduct = productService.getProductById(id);

            existingProduct.setName(updatedProduct.getName());
            existingProduct.setDescription(updatedProduct.getDescription());
            existingProduct.setBrand(updatedProduct.getBrand());
            existingProduct.setPrice(updatedProduct.getPrice());
            existingProduct.getSuppliersList().clear();

            if (supplierIds != null) {
                for (UUID supplierId : supplierIds) {
                    Supplier supplier = supplierService.getSupplierById(supplierId);
                    existingProduct.getSuppliersList().add(supplier);
                }
            }

            productService.updateProduct(existingProduct);

            model.addAttribute("product", existingProduct);
            return "redirect:/product/" + existingProduct.getId();
        }

        model.addAttribute("error", true);
        return "redirect:/admin/products/edit/" + id;
    }
}
