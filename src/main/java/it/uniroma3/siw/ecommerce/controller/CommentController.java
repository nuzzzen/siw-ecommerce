package it.uniroma3.siw.ecommerce.controller;

import it.uniroma3.siw.ecommerce.controller.validator.CommentValidator;
import it.uniroma3.siw.ecommerce.model.Comment;
import it.uniroma3.siw.ecommerce.model.Product;
import it.uniroma3.siw.ecommerce.model.User;
import it.uniroma3.siw.ecommerce.service.CommentService;
import it.uniroma3.siw.ecommerce.service.ProductService;
import it.uniroma3.siw.ecommerce.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.UUID;

@Controller
@RequiredArgsConstructor
@RequestMapping("/comments")
@Log4j2
public class CommentController {

    private final CommentService commentService;
    private final ProductService productService;
    private final UserService userService;
    private final CommentValidator commentValidator;
    private final GlobalController globalController;


    @GetMapping("/newComment/{productId}")
    public String showNewCommentForm(@PathVariable("productId") UUID productId, Model model) {
        Product product = productService.getProductById(productId);
        model.addAttribute("product", product);
        model.addAttribute("comment", new Comment());
        return "product" + product.getId();
    }

    @PostMapping("/newComment/{productId}")
    public String newComment(@Valid @ModelAttribute("comment") Comment comment,
                             BindingResult commentBindingResult,
                             @PathVariable("productId") UUID productId,
                             Model model) {

        commentValidator.validate(comment, commentBindingResult);
        Product product = productService.getProductById(productId);

        String currentUsername = globalController.getUser().getUsername(); // Retrieve the current usernameù
        log.info("Current username: {}", currentUsername);

        if (!commentBindingResult.hasErrors()) {

            User author = userService.getUserByUsername(currentUsername);
            log.info("Current author: {}", author);
            comment.setAuthor(author);
            comment.setProductCommented(product);
            comment.setCreatedAt(Instant.now());
            commentService.saveComment(comment);

            product.getCommentsList().add(comment);
            model.addAttribute("comment", comment);
            return "redirect:/product/" + product.getId();
        }
        return "redirect:/index";

    }


    //TODO finish here
    @PostMapping("/update/{id}")
    public String updateComment(@PathVariable("id") UUID id, @Valid @ModelAttribute("comment") Comment updatedComment,
                                BindingResult commentBindingResult, Model model) {

        Comment existingComment = commentService.getComment(id);
        if (existingComment == null) {
            return "commentNotFound";
        }

        commentValidator.validate(updatedComment, commentBindingResult);

        if (!commentBindingResult.hasErrors()) {
            existingComment.setContent(updatedComment.getContent());
            existingComment.setUpdatedAt(Instant.now());
            commentService.saveComment(existingComment);
            model.addAttribute("comment", existingComment);
            return "comment";
        } else {
            return "formUpdateComment";
        }
    }

    @PostMapping("/delete/{commentId}")
    public String postDeleteComment(@PathVariable("commentId") UUID id) {
        return deleteComment(id);
    }

    @DeleteMapping("/delete/{commentId}")
    public String deleteComment(@PathVariable("commentId") UUID id) {
        Comment comment = commentService.getComment(id);
        Product product = comment.getProductCommented();
        if (comment != null) {
            commentService.deleteComment(id);
        }
        return "redirect:/product/" + product.getId();
    }
}
