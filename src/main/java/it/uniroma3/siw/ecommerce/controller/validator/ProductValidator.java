package it.uniroma3.siw.ecommerce.controller.validator;

import it.uniroma3.siw.ecommerce.model.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.math.BigDecimal;

@Component
@RequiredArgsConstructor
public class ProductValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Product.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "product.name.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "brand", "product.brand.empty");

        Product product = (Product) target;
        BigDecimal price = product.getPrice();
        if (price == null) {
            errors.rejectValue("price", "product.price.null");
        } else if (price.compareTo(BigDecimal.ZERO) <= 0) {
            errors.rejectValue("price", "product.price.invalid");
        }
    }
}

