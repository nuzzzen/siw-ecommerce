package it.uniroma3.siw.ecommerce.controller;

import it.uniroma3.siw.ecommerce.service.ProductService;
import it.uniroma3.siw.ecommerce.service.SupplierService;
import it.uniroma3.siw.ecommerce.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/admin")
public class AdminController {

    private final SupplierService supplierService;
    private final ProductService productService;
    private final UserService userService;


    @GetMapping("/adminDashboard")
    public String adminDashboard() {
        return "admin/adminDashboard";
    }

    @GetMapping("/suppliersDashboard")
    public String suppliersDashboard(Model model) {
        model.addAttribute("suppliers", supplierService.getAllSuppliers());
        return "admin/suppliersDashboard";
    }

    @GetMapping("/productsDashboard")
    public String productDashboard(Model model) {
        model.addAttribute("products", productService.getAllProducts());
        return "admin/productsDashboard";
    }

    @GetMapping("/users")
    public String users(Model model) {
        model.addAttribute("users", userService.getAllUsers());
        return "admin/users";
    }
}