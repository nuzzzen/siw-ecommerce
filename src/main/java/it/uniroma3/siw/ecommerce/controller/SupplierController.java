package it.uniroma3.siw.ecommerce.controller;

import it.uniroma3.siw.ecommerce.model.Supplier;
import it.uniroma3.siw.ecommerce.service.SupplierService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Controller
@RequiredArgsConstructor
public class SupplierController {

    private final SupplierService supplierService;

    @GetMapping("/suppliers")
    public String getAllSuppliers(Model model) {
        model.addAttribute("suppliers", supplierService.getAllSuppliers());
        return "suppliers";
    }

    @GetMapping("/supplier/{id}")
    public String getSupplier(@PathVariable("id") UUID id, Model model) {
        model.addAttribute("supplier", supplierService.getSupplierById(id));
        return "supplier";
    }

    @GetMapping("/admin/createSupplier")
    public String createSupplierForm(Model model) {
        model.addAttribute("supplier", new Supplier());
        return "admin/createSupplier";
    }

    @PostMapping("/admin/suppliers/create")
    public String createSupplier(@Valid @ModelAttribute("supplier") Supplier supplier, BindingResult bindingResult, Model model) {
        if (!bindingResult.hasErrors()) {
            if (!supplierService.existsByNameAndEmail(supplier.getName(), supplier.getEmail())) {
                supplierService.createSupplier(supplier);
                model.addAttribute("supplier", supplier);
                return "redirect:/supplier/" + supplier.getId();
            } else {
                model.addAttribute("error", true);
            }
        }
        return "admin/createSupplier";
    }

    @GetMapping("/admin/suppliers/edit/{id}")
    public String getUpdateSupplierForm(@PathVariable("id") UUID id, Model model) {
        Supplier supplier = supplierService.getSupplierById(id);
        model.addAttribute("supplier", supplier);
        return "admin/editSupplier";
    }

    @PostMapping("/admin/suppliers/edit/{id}")
    public String updateSupplier(@PathVariable("id") UUID id, @ModelAttribute("supplier") Supplier updatedSupplier, Model model) {

        if (supplierService.getSupplierById(id) == null) {
            return "redirect:/admin/suppliersDashboard";
        }

        Supplier supplierToUpdate = supplierService.getSupplierById(id);
        supplierToUpdate.setName(updatedSupplier.getName());
        supplierToUpdate.setEmail(updatedSupplier.getEmail());
        supplierToUpdate.setAddress(updatedSupplier.getAddress());

        supplierService.updateSupplier(supplierToUpdate);

        model.addAttribute("supplier", supplierToUpdate);
        return "redirect:/supplier/" + supplierToUpdate.getId();
    }

    @PostMapping("/admin/suppliers/delete/{id}")
    public String postDeleteSupplier(@PathVariable("id") UUID id) {
        return deleteSupplier(id);
    }

    @DeleteMapping("/admin/suppliers/delete/{id}")
    public String deleteSupplier(@PathVariable("id") UUID id) {
        supplierService.deleteSupplier(id);
        return "redirect:/admin/suppliersDashboard";
    }


}

