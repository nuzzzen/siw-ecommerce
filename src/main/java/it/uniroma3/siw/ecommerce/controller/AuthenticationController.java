package it.uniroma3.siw.ecommerce.controller;

import it.uniroma3.siw.ecommerce.model.User;
import it.uniroma3.siw.ecommerce.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@Log4j2
@RequiredArgsConstructor
public class AuthenticationController {

    private final UserService userService;

    @GetMapping("/index")
    public String home() {
        return "index";
    }

    @GetMapping("/register")
    public String showRegistrationForm(Model model) {
        model.addAttribute("user", new User());
        return "formRegisterUser";
    }

    @PostMapping("/register")
    public String registerUser(
            @Valid @ModelAttribute("user") User user,
            BindingResult bindingResult,
            Model model) {

        if (bindingResult.hasErrors()) {
            return "formRegisterUser";
        }

        if (userService.existsUserByEmail(user.getEmail())) {
            bindingResult.rejectValue("email", "duplicate.email", "Email already exists. Please choose a different one.");
            return "formRegisterUser";
        }

        if (userService.existsUserByUsername(user.getUsername())) {
            bindingResult.rejectValue("username", "duplicate.username", "Username already exists. Please choose a different one.");
            return "formRegisterUser";
        }

        userService.saveUser(user);
        log.info("Registered user: {}", user.getUsername());
        model.addAttribute("success", true);
        return "redirect:/login";
    }


    @GetMapping("/login")
    public String showLoginForm() {
        return "formLogin";
    }
}
