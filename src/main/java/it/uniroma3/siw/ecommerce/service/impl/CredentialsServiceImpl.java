package it.uniroma3.siw.ecommerce.service.impl;

import it.uniroma3.siw.ecommerce.model.Credentials;
import it.uniroma3.siw.ecommerce.repository.CredentialsRepository;
import it.uniroma3.siw.ecommerce.service.CredentialsService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static it.uniroma3.siw.ecommerce.enumeration.users.UserRole.DEFAULT_ROLE;

@Service
@Log4j2
@RequiredArgsConstructor
public class CredentialsServiceImpl implements CredentialsService {

    private final CredentialsRepository credentialsRepository;

    @Override
    public Credentials getCredentialsById(UUID userId) {
        return credentialsRepository
                .findById(userId)
                .orElseThrow(() -> new EntityNotFoundException(
                        "No credentials found for user with ID: %s".formatted(userId))
                );
    }

    @Override
    public Credentials getCredentialsByUsername(String username) {
        return credentialsRepository
                .findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException(
                        "No credentials found for user: %s".formatted(username))
                );
    }

    @Override
    public Credentials saveCredentials(Credentials credentials) {
        credentials.setUserRole(DEFAULT_ROLE);
//        credentials.setPassword(new BCryptPasswordEncoder().encode(credentials.getPassword()));
        credentials.setPassword(credentials.getPassword());

        return credentialsRepository.save(credentials);
    }
}
