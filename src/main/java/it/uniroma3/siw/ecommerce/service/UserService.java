package it.uniroma3.siw.ecommerce.service;

import it.uniroma3.siw.ecommerce.model.Comment;
import it.uniroma3.siw.ecommerce.model.User;

import java.util.List;
import java.util.UUID;


public interface UserService {
    List<User> getAllUsers();

    User getUserByEmail(String email);

    Boolean existsUserByEmail(String email);

    Boolean existsUserByUsername(String username);

    User getUserByUsername(String username);

    List<Comment> getUserCommentList(UUID userId);

    void saveUser(User user);

    void updateUser(User user);

    void deleteUser(UUID userId);


}
