package it.uniroma3.siw.ecommerce.service;

import it.uniroma3.siw.ecommerce.model.Credentials;

import java.util.UUID;

public interface CredentialsService {
    Credentials getCredentialsById(UUID userId);

    Credentials getCredentialsByUsername(String username);

    Credentials saveCredentials(Credentials credentials);
}
