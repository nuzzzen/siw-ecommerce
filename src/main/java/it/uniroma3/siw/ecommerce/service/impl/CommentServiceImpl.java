package it.uniroma3.siw.ecommerce.service.impl;

import it.uniroma3.siw.ecommerce.model.Comment;
import it.uniroma3.siw.ecommerce.repository.CommentRepository;
import it.uniroma3.siw.ecommerce.repository.UserRepository;
import it.uniroma3.siw.ecommerce.service.CommentService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final UserRepository userRepository;

    @Override
    public List<Comment> getCommentsByAuthor(String username) {
        return userRepository
                .findUserByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException("Cannot find user : %s".formatted(username)))
                .getAuthoredCommentsList();
    }

    @Override
    public Comment getComment(UUID id) {
        return commentRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Cannot find comment with ID: %s".formatted(id)));
    }

    @Override
    public void saveComment(Comment comment) {
        commentRepository.save(comment);
    }

    @Override
    public void deleteComment(UUID commentUuid) {
        commentRepository.deleteById(commentUuid);
    }

    @Override
    public List<Comment> getAllComments() {
        return commentRepository.findAll();
    }
}

