package it.uniroma3.siw.ecommerce.service.impl;

import it.uniroma3.siw.ecommerce.model.Comment;
import it.uniroma3.siw.ecommerce.model.User;
import it.uniroma3.siw.ecommerce.repository.UserRepository;
import it.uniroma3.siw.ecommerce.service.UserService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static it.uniroma3.siw.ecommerce.enumeration.users.UserRole.DEFAULT_ROLE;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository
                .findUserByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Cannot find any user associated to %s email.".formatted(email))
                );
    }

    @Override
    public Boolean existsUserByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public Boolean existsUserByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository
                .findUserByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Cannot find any user associated to %s username.".formatted(username))
                );
    }

    @Override
    public List<Comment> getUserCommentList(UUID userId) {
        return userRepository
                .findUserById(userId)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Cannot find any user associated to %s username.".formatted(userId)))
                .getAuthoredCommentsList();
    }

    @Override
    public void saveUser(User user) {

        if (user.getUserRole() == null) {
            user.setUserRole(DEFAULT_ROLE);
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public void updateUser(User user) {
        userRepository.save(user);
    }

    @Override
    public void deleteUser(UUID userId) {
        userRepository.deleteById(userId);
    }
}
