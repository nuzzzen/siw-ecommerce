package it.uniroma3.siw.ecommerce.service;

import it.uniroma3.siw.ecommerce.model.Product;
import it.uniroma3.siw.ecommerce.model.Supplier;

import java.util.List;
import java.util.UUID;

public interface ProductService {
    List<Product> getAllProducts();

    Product getProductByBrandAndName(String brand, String name);

    Product getProductByName(String name);

    Product getProductById(UUID productId);

    List<Supplier> getSupplierListByProduct(UUID productId);

    void createProduct(Product product);

    void updateProduct(Product product);

    void deleteProduct(UUID productId);


    void addSupplierToProduct(Product product, UUID supplierId);

    void removeSupplierFromProduct(Product product, UUID supplierId);
}
