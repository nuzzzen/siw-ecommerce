package it.uniroma3.siw.ecommerce.service.impl;

import it.uniroma3.siw.ecommerce.model.Product;
import it.uniroma3.siw.ecommerce.model.Supplier;
import it.uniroma3.siw.ecommerce.repository.ProductRepository;
import it.uniroma3.siw.ecommerce.repository.SupplierRepository;
import it.uniroma3.siw.ecommerce.service.SupplierService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class SupplierServiceImpl implements SupplierService {

    private final SupplierRepository supplierRepository;
    private final ProductRepository productRepository;

    @Override
    public List<Supplier> getAllSuppliers() {
        return supplierRepository.findAll();
    }

    @Override
    public Supplier getSupplierById(UUID id) {
        return supplierRepository
                .findSupplierById(id)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Cannot find supplier with ID %s".formatted(id))
                );
    }

    @Override
    public Supplier getSupplierByName(String name) {
        return supplierRepository
                .findByName(name)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Cannot find supplier %s".formatted(name))
                );
    }

    @Override
    public List<Product> getSupplierProductList(UUID supplierId) {
        Supplier supplier = supplierRepository
                .findSupplierById(supplierId)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Cannot find supplier with ID %s".formatted(supplierId)));
        return supplier.getProductsList();
    }

    @Override
    public void removeProductFromSupplier(Supplier supplier, UUID productId) {
        Optional<Product> productOptional = productRepository.findById(productId);

        if (productOptional.isPresent()) {
            Product product = productOptional.get();
            supplier.getProductsList().remove(product);
            product.getSuppliersList().remove(supplier);
            productRepository.save(product);
            supplierRepository.save(supplier);
        } else {
            throw new EntityNotFoundException("Product not found with ID: %s".formatted(productId));
        }

    }

    @Override
    public void createSupplier(Supplier supplier) {
        supplierRepository.save(supplier);
    }

    @Override
    public void updateSupplier(Supplier supplier) {
        Supplier supplierToUpdate = supplierRepository
                .findSupplierById(supplier.getId())
                .orElseThrow(() -> new EntityNotFoundException(
                        "Cannot find supplier with ID %s".formatted(supplier.getId())));
        supplierRepository.save(supplierToUpdate);
    }

    @Override
    public void deleteSupplier(UUID supplierId) {
        List<Product> supplierProductList = getSupplierProductList(supplierId);
        for (Product product : supplierProductList) {
            product.getSuppliersList().remove(getSupplierById(supplierId));
            productRepository.save(product);
        }
        supplierRepository.deleteById(supplierId);
    }

    @Override
    public boolean existsByNameAndEmail(String name, String email) {
        return supplierRepository.existsByNameAndEmail(name, email);
    }
}
