package it.uniroma3.siw.ecommerce.service;

import it.uniroma3.siw.ecommerce.model.Product;
import it.uniroma3.siw.ecommerce.model.Supplier;

import java.util.List;
import java.util.UUID;

public interface SupplierService {
    List<Supplier> getAllSuppliers();

    Supplier getSupplierById(UUID id);


    Supplier getSupplierByName(String name);

    List<Product> getSupplierProductList(UUID supplierId);

    void removeProductFromSupplier(Supplier supplier, UUID productId);

    void createSupplier(Supplier supplier);

    void updateSupplier(Supplier supplier);

    void deleteSupplier(UUID supplierId);

    boolean existsByNameAndEmail(String name, String email);
}
