package it.uniroma3.siw.ecommerce.service.impl;

import it.uniroma3.siw.ecommerce.model.Product;
import it.uniroma3.siw.ecommerce.model.Supplier;
import it.uniroma3.siw.ecommerce.repository.ProductRepository;
import it.uniroma3.siw.ecommerce.repository.SupplierRepository;
import it.uniroma3.siw.ecommerce.service.ProductService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final SupplierRepository supplierRepository;

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public Product getProductByBrandAndName(String brand, String name) {
        return productRepository.findByBrandAndName(brand, name)
                .orElseThrow(() -> new EntityNotFoundException(
                                "Cannot find product %s from brand %s.".formatted(name, brand))
        );
    }

    @Override
    public Product getProductByName(String name) {
        return productRepository.findProductByName(name)
                .orElseThrow(() -> new EntityNotFoundException(
                                "Cannot find product %s.".formatted(name))
        );
    }

    @Override
    public Product getProductById(UUID productId) {
        return productRepository.findById(productId)
                .orElseThrow(() -> new EntityNotFoundException(
                        "Cannot find product with ID %s.".formatted(productId))
                );
    }

    @Override
    public List<Supplier> getSupplierListByProduct(UUID productId) {
        return getProductById(productId).getSuppliersList();
    }

    @Override
    public void createProduct(Product product) {
        productRepository.save(product);
    }

    @Override
    public void updateProduct(Product product) {
        Product productToUpdate = productRepository
                .findById(product.getId())
                .orElseThrow(() -> new EntityNotFoundException(
                        "Cannot find product with ID %s.".formatted(product.getId())));
        productRepository.save(productToUpdate);
    }

    @Override
    public void deleteProduct(UUID productId) {
        productRepository.deleteById(productId);
    }

    @Override
    public void addSupplierToProduct(Product product, UUID supplierId) {
        Optional<Supplier> supplierOptional = supplierRepository.findSupplierById(supplierId);

        if (supplierOptional.isPresent()) {
            Supplier supplier = supplierOptional.get();
            product.getSuppliersList().add(supplier);
            supplier.getProductsList().add(product);
            productRepository.save(product);
            supplierRepository.save(supplier);
        } else throw new EntityNotFoundException("Supplier not found with ID: %s".formatted(supplierId));
    }

    @Override
    public void removeSupplierFromProduct(Product product, UUID supplierId) {
        Optional<Supplier> supplierOptional = supplierRepository.findSupplierById(supplierId);

        if (supplierOptional.isPresent()) {
            Supplier supplier = supplierOptional.get();
            product.getSuppliersList().remove(supplier);
            supplier.getProductsList().remove(product);
            productRepository.save(product);
            supplierRepository.save(supplier);
        } else {
            throw new EntityNotFoundException("Supplier not found with ID: %s".formatted(supplierId));
        }
    }
}
