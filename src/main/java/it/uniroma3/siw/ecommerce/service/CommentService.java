package it.uniroma3.siw.ecommerce.service;

import it.uniroma3.siw.ecommerce.model.Comment;

import java.util.List;
import java.util.UUID;

public interface CommentService {

    List<Comment> getCommentsByAuthor(String username);

    Comment getComment(UUID id);

    void saveComment(Comment comment);

    void deleteComment(UUID commentUuid);


    List<Comment> getAllComments();
}
