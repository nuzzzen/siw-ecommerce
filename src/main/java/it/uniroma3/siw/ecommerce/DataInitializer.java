package it.uniroma3.siw.ecommerce;

import it.uniroma3.siw.ecommerce.model.*;
import it.uniroma3.siw.ecommerce.repository.*;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static it.uniroma3.siw.ecommerce.enumeration.users.UserRole.ADMIN_ROLE;
import static it.uniroma3.siw.ecommerce.enumeration.users.UserRole.DEFAULT_ROLE;

@RequiredArgsConstructor
public class DataInitializer {

    private final ProductRepository productRepository;
    private final SupplierRepository supplierRepository;
    private final UserRepository userRepository;
    private final CredentialsRepository credentialsRepository;
    private final CommentRepository commentRepository;
    private final PasswordEncoder passwordEncoder;


    public void populateDatabase() {
        createSuppliers();

        createProducts();

        List<Supplier> allSuppliers = supplierRepository.findAll();
        List<Product> allProducts = productRepository.findAll();

        for (Product product : allProducts) {
            for (Supplier supplier : allSuppliers) {
               supplier.getProductsList().add(product);
               product.getSuppliersList().add(supplier);
            }
        }

        productRepository.saveAll(allProducts);
        supplierRepository.saveAll(allSuppliers);

        createComment();
    }

    /**
     * Create and save a comment by the default user
     */
    private void createComment() {
        User user = userRepository.findUserByEmail("user@example.com").get();
        Product product = productRepository.findByBrandAndName("Brand X", "Product 1").get();

        Comment comment = new Comment();
        comment.setAuthor(user);
        comment.setContent("This is a comment by the default user.");
        comment.setProductCommented(product);
        product.getCommentsList().add(comment);
        productRepository.save(product);
        commentRepository.save(comment);
    }


    /**
     * Create and save products
     */
    private void createProducts() {
        List<Product> products = new ArrayList<>();
        products.add(new Product(UUID.randomUUID(), "Brand A", "Product 1", new BigDecimal("30.00"), "Description for Product 4", new ArrayList<>(), new ArrayList<>()));
        products.add(new Product(UUID.randomUUID(), "Brand B", "Product 2", new BigDecimal("10.00"), "Description for Product 5", new ArrayList<>(), new ArrayList<>()));
        products.add(new Product(UUID.randomUUID(), "Brand C", "Product 3", new BigDecimal("20.00"), "Description for Product 6", new ArrayList<>(), new ArrayList<>()));
        productRepository.saveAll(products);
    }

    /**
     * Create and save suppliers
     */
    private void createSuppliers() {
        List<Supplier> suppliers = new ArrayList<>();
        suppliers.add(new Supplier(UUID.randomUUID(), "Supplier F", "supplierF@example.com", "Address for Supplier F", new ArrayList<>()));
        suppliers.add(new Supplier(UUID.randomUUID(), "Supplier G", "supplierG@example.com", "Address for Supplier G", new ArrayList<>()));
        suppliers.add(new Supplier(UUID.randomUUID(), "Supplier H", "supplierH@example.com", "Address for Supplier H", new ArrayList<>()));
        supplierRepository.saveAll(suppliers);
    }
}
