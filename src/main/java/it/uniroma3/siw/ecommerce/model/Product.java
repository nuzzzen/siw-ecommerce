package it.uniroma3.siw.ecommerce.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;


@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(nullable = false, length = 100)
    private String brand;

    @Column(nullable = false, length = 100)
    private String name;

    @Min(0)
    @Column(nullable = false)
    private BigDecimal price;

    @Column(length = 400)
    private String description;

    @ManyToMany(targetEntity = Supplier.class)
    private List<Supplier> suppliersList;

    @OneToMany(mappedBy = "productCommented", cascade = CascadeType.ALL)
    private List<Comment> commentsList;
}
